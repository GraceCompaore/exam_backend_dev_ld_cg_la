





insert into cars (mark, registration, year_of_construction)
values ('Kia','CMN6288956',2002);
insert into cars ( mark, registration, year_of_construction)
values ('Clio','A3658994774',2006);
insert into cars ( mark, registration, year_of_construction)
values ('Ford','BN253525568',1998);
insert into cars ( mark, registration, year_of_construction)
values ('Clio','A3658994774',2006);
insert into cars ( mark, registration, year_of_construction)
values ('Ford','BN253525568',1998);

insert into technicians (age, firstname, lastname, address_id, car_id, manager_id)
values (35,'Laurent','Pedrot',1,2,3);
insert into technicians ( age, firstname, lastname, address_id, car_id, manager_id)
values (40,'Mathieu','Benjamin',2,3,4);
insert into technicians ( age, firstname, lastname, address_id, car_id, manager_id)
values (27,'Valerie','Raymond',4,5,2);
insert into technicians (age, firstname, lastname, address_id, car_id, manager_id)
values (32,'Guillaume','Martin',3,2,1);
insert into technicians ( age, firstname, lastname, address_id, car_id, manager_id)
values (55,'Anne','Marie',5,4,3);

insert into worksites (name, price, address_id)
values ('Maison',12000,1);
insert into worksites ( name, price, address_id)
values ('Theatre',160000,2);
insert into worksites ( name, price, address_id)
values ('Eglise', 325000,3);
insert into worksites (name, price, address_id)
values ('Restaurant', 369785, 4);
insert into worksites ( name, price, address_id)
values ('Renovation publique', 89512, 5);

insert into managers (firstname, lastname, mobile, phone)
values ('Obi-Wan', 'KENOBI', 0600000000, 0200000000);

insert into managers (firstname, lastname, mobile, phone)
values ('Marty', 'MCFLY', 0600000000, 0200000000);

insert into managers (firstname, lastname, mobile, phone)
values ('John', 'MACCLANE', 0600000000, 0200000000);

insert into managers (firstname, lastname, mobile, phone)
values ('TOM', 'JERRY', 0600000000, 0200000000);

insert into managers (firstname, lastname, mobile, phone)
values ('Luna', 'JORRY', 0600000000, 0200000000);


insert into addresses (city_name, number, street)
values ('Paris', 10, 'Rue de la paix');

insert into addresses (city_name, number, street)
values ('Rennes', 10, 'Rue de la paix');

insert into addresses (city_name, number, street)
values ('Tours', 10, 'Rue de la paix');

insert into addresses (city_name, number, street)
values ('Nantes', 10, 'Rue de la paix');

insert into addresses (city_name, number, street)
values ('Lille', 10, 'Rue de la paix');

insert into users (grants, password, username)
values ('ADMIN', '$2y$10$345CRVS.DVjr5HpWwqWdLOfenFvpNHxTUOUf.sgH7aTIgeqA.Xst2', 'admin' );

insert into users (grants, password, username)
values ('USER', '$2y$10$6.9eYEs4Rq5xzTUHFpjOPupgtpIhqTrQAVBkLeebCUZcfcX/d4zRy', 'user' );