package com.example.Building.Co.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Bean
    public UserDetailsService userDetailsService(){
        return new MyUserDetailsServiceImpl();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    @Bean
    public DaoAuthenticationProvider authenticationProvider (){
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setPasswordEncoder(passwordEncoder());
        authProvider.setUserDetailsService(userDetailsService());
        return authProvider;
    }

        @Override
        public void configure(AuthenticationManagerBuilder authenticationManagerBuilder){
            authenticationManagerBuilder.authenticationProvider(authenticationProvider());
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.httpBasic().and().authorizeRequests()
                    .antMatchers(HttpMethod.POST, "/v1/technicians/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.PUT, "/v1/technicians/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/v1/cars/**").hasRole("USER")
                    .antMatchers(HttpMethod.POST, "/v1/users/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/v1/addresses/**").hasRole("USER")
                    .antMatchers(HttpMethod.POST, "/v1/managers/**").hasRole("ADMIN")
                    .antMatchers(HttpMethod.POST, "/v1/worksites/**").hasRole("ADMIN")
                    .anyRequest().authenticated();

        }

}
