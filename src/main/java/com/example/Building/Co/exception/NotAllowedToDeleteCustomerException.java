package com.example.Building.Co.exception;

public class NotAllowedToDeleteCustomerException extends RuntimeException{

    public NotAllowedToDeleteCustomerException(String message) {
        super(message);
    }

    public NotAllowedToDeleteCustomerException() {
        super("Cannot delete the given customer");
    }
}
