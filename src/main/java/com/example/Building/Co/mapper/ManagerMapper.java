package com.example.Building.Co.mapper;


import com.example.Building.Co.api.dto.ManagerDto;
import com.example.Building.Co.entity.Manager;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ManagerMapper {


    ManagerDto mapToManagerDto(Manager manager);


    Manager mapToModel(ManagerDto managerDto);
}
