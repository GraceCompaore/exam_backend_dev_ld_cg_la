package com.example.Building.Co.mapper;


import com.example.Building.Co.api.dto.WorksiteDto;
import com.example.Building.Co.entity.Worksite;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface WorksiteMapper {

    @Mapping(source = "address.id", target = "addressId")
    WorksiteDto mapToWorksiteDto(Worksite worksite);


    @Mapping(source = "addressId", target = "address.id")
    Worksite mapToModel(WorksiteDto worksiteDto);
}
