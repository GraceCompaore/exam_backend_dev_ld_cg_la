package com.example.Building.Co.mapper;


import com.example.Building.Co.api.dto.CarDto;
import com.example.Building.Co.api.dto.TechnicianDto;
import com.example.Building.Co.entity.Car;
import com.example.Building.Co.entity.Technician;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface TechnicianMapper {

    @Mapping(target = "addressId", source = "address.id")
    @Mapping(target = "managerId", source = "manager.id")
    @Mapping(target = "carId", source = "car.id")
    TechnicianDto mapToDto(Technician technician);

    @Mapping(source = "addressId", target = "address.id")
    @Mapping(source = "managerId", target = "manager.id")
    @Mapping(source = "carId", target = "car.id")
    Technician mapToModel(TechnicianDto technicianDto);
}
