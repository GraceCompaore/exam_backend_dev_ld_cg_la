package com.example.Building.Co.mapper;


import com.example.Building.Co.api.dto.CarDto;
import com.example.Building.Co.entity.Car;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CarMapper {

    @Mapping(source="technician.id", target="technicianId")
    CarDto mapToCarDto(Car car);

    @Mapping(source="technicianId", target="technician.id")
    Car mapToModel(CarDto carDto);
}
