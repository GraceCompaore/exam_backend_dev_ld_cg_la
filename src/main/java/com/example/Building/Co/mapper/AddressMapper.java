package com.example.Building.Co.mapper;


import com.example.Building.Co.api.dto.AddressDto;
import com.example.Building.Co.entity.Address;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.springframework.stereotype.Component;

@Component
@Mapper(componentModel = "spring", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface AddressMapper {

    AddressDto mapToAdressDto(Address address);

    Address mapToModel(AddressDto addressDto);
}
