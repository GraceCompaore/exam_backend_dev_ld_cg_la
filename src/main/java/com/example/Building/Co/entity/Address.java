package com.example.Building.Co.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Table(name="addresses")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "city_name")
    private String cityName;

    @Column(name="street")
    private String street;

    @Column(name="number")
    private Integer number;

    @OneToMany(mappedBy = "address")
    private Set<Technician> technicians;

   @OneToMany(mappedBy = "address")
    private Set<Worksite> worksites;
}
