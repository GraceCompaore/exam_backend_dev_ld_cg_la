package com.example.Building.Co.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Table(name="worksites")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Worksite {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String name;

    private Double price;

    @ManyToMany
    @JoinTable(name = "technician_worksite",
            joinColumns = {@JoinColumn(name="worksite_id")},
            inverseJoinColumns = {@JoinColumn(name="technician_id")})
    private Set<Technician> technicians;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="address_id")
    private Address address;

}
