package com.example.Building.Co.entity;


import lombok.Data;

import javax.persistence.*;

@Table(name="users")
@Entity
@Data

public class User {
    @Id
    @Column(name = "id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String username;

    private String password;

    private String grants;


}
