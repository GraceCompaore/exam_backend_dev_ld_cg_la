package com.example.Building.Co.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Table(name="technicians")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Technician {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name="lastname")
    private String lastname;

    @Column(name="firstname")
    private String firstname;

    @Column(length = 2)
    private Integer age;

    @ManyToOne(fetch=FetchType.LAZY)
    @JoinColumn(name="manager_id", nullable = false)
    private Manager manager;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    private Car car;

    @ManyToMany(mappedBy = "technicians")
    private List<Worksite> worksites;

}
