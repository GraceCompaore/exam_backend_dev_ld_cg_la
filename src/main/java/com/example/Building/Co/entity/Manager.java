package com.example.Building.Co.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Table(name="managers")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Manager {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(length = 100)
    private String lastname;

    @Column(length = 100)
    private String firstname;

    @Column( length = 15)
    private String phone;

    @Column( length = 15)
    private String mobile;

    @OneToMany(mappedBy = "manager")
    private Set<Technician> technicians;

}
