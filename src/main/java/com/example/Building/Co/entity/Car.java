package com.example.Building.Co.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Table(name="cars")
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    @Column(name = "registration", nullable = false)
    private String registration;

    @Column(name = "mark", nullable = false)
    private String mark;

    @Column(name = "year_of_construction", nullable = false)
    private Integer yearOfConstuction;

    @OneToOne(mappedBy = "car")
    private Technician technician;
}
