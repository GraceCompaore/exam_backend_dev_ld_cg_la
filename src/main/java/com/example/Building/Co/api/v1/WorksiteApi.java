package com.example.Building.Co.api.v1;

import com.example.Building.Co.api.dto.WorksiteDto;
import com.example.Building.Co.exception.NotAllowedToDeleteCustomerException;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.mapper.WorksiteMapper;
import com.example.Building.Co.service.WorksiteService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/worksites")


public class WorksiteApi {

    private final WorksiteService worksiteService;
    private final WorksiteMapper worksiteMapper;

    public WorksiteApi(WorksiteService worksiteService, WorksiteMapper worksiteMapper) {
        this.worksiteService = worksiteService;
        this.worksiteMapper = worksiteMapper;
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "Trying to retrieve an worksite from the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the worksite found the given ID"),
            @ApiResponse(responseCode = "404", description = "No worksite found the given ID")
    })
    public ResponseEntity<WorksiteDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.worksiteMapper
                    .mapToWorksiteDto(this.worksiteService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return the list of worksites")
    public ResponseEntity<List<WorksiteDto>> getAll() {

        return ResponseEntity.ok(
                this.worksiteService.getAll().stream()
                        .map(this.worksiteMapper::mapToWorksiteDto)
                        .toList()
        );
    }



    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create an worksite")
    @ApiResponse(responseCode = "201", description = "Created")

    public ResponseEntity<WorksiteDto> createWorksite(@RequestBody final WorksiteDto worksiteDto) {

        WorksiteDto worksiteDtoResponse =
                this.worksiteMapper.mapToWorksiteDto(
                        this.worksiteService.createWorksite(
                                this.worksiteMapper.mapToModel(worksiteDto)
                        )
                );

        return ResponseEntity.created(URI.create("/v1/worksites/" + worksiteDtoResponse.getId()))
                .body(worksiteDtoResponse);
    }
    @DeleteMapping(path= "/{id}")
    @Operation(summary = "Delete an worksite for the given ID" )
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "403", description = "Cannot delete the worksite for the given ID"),
            @ApiResponse(responseCode = "404", description = "No worksite found the given ID")
    })

    public ResponseEntity<Void> deleteWorksite(@PathVariable final Integer id){
        try{
            this.worksiteService.deleteWorksite(id);
            return ResponseEntity.noContent().build();

        } catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());

        } catch(NotAllowedToDeleteCustomerException ex){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());

        }
    }

    @PutMapping(path="/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary= "update an worksite")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "no content")
    })

    public ResponseEntity<Void> updateWorksite(@PathVariable final Integer id,
                                            @RequestBody WorksiteDto worksiteDto) {
        try {
            worksiteDto.setId(id);
            this.worksiteService.updateWorksite(worksiteMapper.mapToModel(worksiteDto));

            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }
}
