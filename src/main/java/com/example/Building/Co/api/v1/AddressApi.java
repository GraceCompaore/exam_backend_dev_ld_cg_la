package com.example.Building.Co.api.v1;

import com.example.Building.Co.api.dto.AddressDto;
import com.example.Building.Co.exception.NotAllowedToDeleteCustomerException;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.mapper.AddressMapper;
import com.example.Building.Co.service.AddressService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;


@RestController
@RequestMapping("/v1/addresses")
@Data
@AllArgsConstructor
public class AddressApi {

    private final AddressService addressService;
    private final AddressMapper addressMapper;

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(
            summary = "Return the list of all addresses "
    )
    public ResponseEntity<List<AddressDto>> getAll() {
        return ResponseEntity.ok(
                this.addressService.getAll().stream()
                        .map(this.addressMapper::mapToAdressDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "Trying to retrieve an address from the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the address found the given ID"),
            @ApiResponse(responseCode = "404", description = "No address found the given ID")
    })
    public ResponseEntity<AddressDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.addressMapper
                    .mapToAdressDto(this.addressService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }


    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create an address")
    @ApiResponse(responseCode = "201", description = "Created")

    public ResponseEntity<AddressDto> createAddress(@RequestBody final AddressDto addressDto) {

        AddressDto addressDtoResponse =
                this.addressMapper.mapToAdressDto(
                        this.addressService.createAddress(
                                this.addressMapper.mapToModel(addressDto)

                        )
                );

        return ResponseEntity.created(URI.create("/v1/addresses/" + addressDtoResponse.getId()))
                .body(addressDtoResponse);
    }

    @DeleteMapping(path= "/{id}")
    @Operation(summary = "Delete an address for the given ID" )
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "403", description = "Cannot delete the adresses for the given ID"),
            @ApiResponse(responseCode = "404", description = "No customer found the given ID")
    })

    public ResponseEntity<Void> deleteAddress(@PathVariable final Integer id){

        try{
            this.addressService.deleteAddress(id);
            return ResponseEntity.noContent().build();

        } catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());

        } catch(NotAllowedToDeleteCustomerException ex){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());

        }
    }

    @PutMapping(path="/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary= "update a address")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "no content")
    })

    public ResponseEntity<Void> updateAddress(@PathVariable final Integer id,
                                               @RequestBody AddressDto addressDto){
        try{
            addressDto.setId(id);
            this.addressService.updateAddress(addressMapper.mapToModel(addressDto));
            return ResponseEntity.noContent().build();
        }catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }

    }
}
