package com.example.Building.Co.api.dto;


import com.example.Building.Co.entity.Address;
import com.example.Building.Co.entity.Car;
import com.example.Building.Co.entity.Manager;
import com.example.Building.Co.entity.Worksite;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TechnicianDto {

    private Integer id;
    private String firstname;
    private String lastname;
    private Integer age;
    private Integer addressId;
    private Integer managerId;
    private Integer carId;


}
