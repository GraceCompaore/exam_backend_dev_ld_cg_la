package com.example.Building.Co.api.dto;

import com.example.Building.Co.entity.Technician;
import com.example.Building.Co.entity.Worksite;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {

    private Integer id;

    private String cityName;

    private String street;

    private Integer number;

}
