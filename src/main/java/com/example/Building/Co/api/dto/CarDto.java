package com.example.Building.Co.api.dto;

import com.example.Building.Co.entity.Technician;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarDto {
    private Integer id;

    private String registration;

    private String mark;

    private Integer yearOfConstuction;

    private Integer technicianId;
}
