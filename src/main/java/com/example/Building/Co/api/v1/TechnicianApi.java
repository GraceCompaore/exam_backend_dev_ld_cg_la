package com.example.Building.Co.api.v1;

import com.example.Building.Co.api.dto.TechnicianDto;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.mapper.TechnicianMapper;
import com.example.Building.Co.service.TechnicianService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/technicians")
public class TechnicianApi {
    Logger log = LoggerFactory.getLogger(AddressApi.class);
    private final TechnicianService technicianService;
    private final TechnicianMapper technicianMapper;

    public TechnicianApi(TechnicianService technicianService, TechnicianMapper technicianMapper) {
        this.technicianService = technicianService;
        this.technicianMapper = technicianMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "return the list of technicians.")
    public ResponseEntity<List<TechnicianDto>> getAll() {
        return ResponseEntity.ok(
                this.technicianService.getAll().stream()
                        .map(this.technicianMapper::mapToDto)
                        .toList()
        );
    }

    @GetMapping(path = "/{id}", produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Return the technician for the specified id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Returned the technician found at the given ID"),
            @ApiResponse(responseCode = "404", description = "No technician  found at the given ID")
    })
    public ResponseEntity<TechnicianDto> getById(@PathVariable final Integer id) {
        try {

            return ResponseEntity.ok(
                    this.technicianMapper.mapToDto(this.technicianService.getById(id))
            );
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PostMapping(consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(summary = "Create a new technician")
    @ApiResponse(responseCode = "201", description = "created")
    public ResponseEntity<TechnicianDto> createTechnician(@RequestBody final TechnicianDto technicianDto) {
        TechnicianDto technicianDtoResponse = this.technicianMapper.mapToDto(
                this.technicianService.createTechnician(
                        this.technicianMapper.mapToModel(technicianDto)
                )
        );
        return ResponseEntity
                .created(URI.create("v1/technicians" + technicianDtoResponse.getId()))
                .body(technicianDtoResponse);
    }

    @DeleteMapping(path = "/{id}")
    @Operation(summary = "Delete an technician")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "404", description = "No technician found for the given ID"),
    })
    public ResponseEntity<Void> deleteTechnician(@PathVariable final Integer id) {
        try {
            this.technicianService.deleteTechnician(id);
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

    @PutMapping(path = "/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Modify a technician")
    @ApiResponse(responseCode = "204", description = "No content.")
    public ResponseEntity<TechnicianDto> updateTechnician(@PathVariable final Integer id, @RequestBody TechnicianDto technicianDto) {
        try {
            technicianDto.setId(id);
            this.technicianService.updateTechnician(technicianMapper.mapToModel(technicianDto));
            return ResponseEntity.noContent().build();
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }

}
