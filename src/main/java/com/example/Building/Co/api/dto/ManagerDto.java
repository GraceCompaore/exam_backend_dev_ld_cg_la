package com.example.Building.Co.api.dto;

import com.example.Building.Co.entity.Technician;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManagerDto {
    private Integer id;

    private String lastname;

    private String firstname;

    private String phone;

    private String mobile;

}
