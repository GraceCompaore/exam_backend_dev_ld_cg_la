package com.example.Building.Co.api.v1;

import com.example.Building.Co.api.dto.AddressDto;
import com.example.Building.Co.api.dto.CarDto;
import com.example.Building.Co.exception.NotAllowedToDeleteCustomerException;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.mapper.CarMapper;
import com.example.Building.Co.service.CarService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/v1/cars")
public class CarApi {
    
    private final CarService carService;
    private final CarMapper carMapper;

    public CarApi(CarService carService, CarMapper carMapper) {
        this.carService = carService;
        this.carMapper = carMapper;
    }

    @GetMapping(produces = {MediaType.APPLICATION_JSON_VALUE})
    @Operation(
            summary = "Return the list of all cars "
    )
    public ResponseEntity<List<CarDto>> getAll() {
        return ResponseEntity.ok(
                this.carService.getAll().stream()
                        .map(this.carMapper::mapToCarDto)
                        .toList()
        );
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "Trying to retrieve an car from the given ID")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "Return the address found the given ID"),
            @ApiResponse(responseCode = "404", description = "No address found the given ID")
    })
    public ResponseEntity<CarDto> getById(@PathVariable Integer id) {
        try {
            return ResponseEntity.ok(this.carMapper
                    .mapToCarDto(this.carService.getById(id)));
        } catch (UnknownResourceException ure) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }
    }


    @PostMapping(
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE}
    )
    @Operation(summary = "Create an car")
    @ApiResponse(responseCode = "201", description = "Created")

    public ResponseEntity<CarDto> createCar(@RequestBody final CarDto carDto) {

        CarDto carDtoResponse =
                this.carMapper.mapToCarDto(
                        this.carService.createCar(
                                this.carMapper.mapToModel(carDto)

                        )
                );

        return ResponseEntity.created(URI.create("/v1/cars/" + carDtoResponse.getId()))
                .body(carDtoResponse);
    }

    @DeleteMapping(path= "/{id}")
    @Operation(summary = "Delete an car for the given ID" )
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "No content"),
            @ApiResponse(responseCode = "403", description = "Cannot delete the adresses for the given ID"),
            @ApiResponse(responseCode = "404", description = "No customer found the given ID")
    })

    public ResponseEntity<Void> deleteCar(@PathVariable final Integer id){

        try{
            this.carService.deleteCar(id);
            return ResponseEntity.noContent().build();

        } catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());

        } catch(NotAllowedToDeleteCustomerException ex){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());

        }
    }

    @PutMapping(path="/{id}", consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE, MediaType.TEXT_PLAIN_VALUE})
    @Operation(summary= "update a address")
    @ApiResponses({
            @ApiResponse(responseCode = "204", description = "no content")
    })

    public ResponseEntity<Void> updateCar(@PathVariable final Integer id,
                                              @RequestBody CarDto carDto){
        try{
            carDto.setId(id);
            this.carService.updateCar(carMapper.mapToModel(carDto));
            return ResponseEntity.noContent().build();
        }catch(UnknownResourceException ure){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, ure.getMessage());
        }

    }
    
}
