package com.example.Building.Co.api.dto;

import com.example.Building.Co.entity.Address;
import com.example.Building.Co.entity.Technician;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WorksiteDto {

    private Integer id;

    private String name;

    private Double price;

    private Integer addressId;

/*    private List<Integer> technicianId;*/
}
