package com.example.Building.Co.Repository;

import com.example.Building.Co.entity.Worksite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorksiteRepository extends JpaRepository<Worksite, Integer> {
}
