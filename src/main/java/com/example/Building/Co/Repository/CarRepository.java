package com.example.Building.Co.Repository;

import com.example.Building.Co.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarRepository extends JpaRepository<Car, Integer> {
}
