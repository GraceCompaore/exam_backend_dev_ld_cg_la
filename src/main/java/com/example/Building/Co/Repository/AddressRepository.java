package com.example.Building.Co.Repository;

import com.example.Building.Co.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Integer> {
}
