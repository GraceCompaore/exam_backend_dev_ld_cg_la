package com.example.Building.Co.service.serviceImpl;

import com.example.Building.Co.Repository.CarRepository;
import com.example.Building.Co.entity.Car;
import com.example.Building.Co.entity.Car;
import com.example.Building.Co.entity.Technician;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.service.CarService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    @Autowired
    private CarRepository carRepository;
    
    @Override
    public Car getById(Integer id) {
        
        return this.carRepository
                .findById(id)
                .orElseThrow(()-> new UnknownResourceException("No car found by this id"));
    }

    @Override
    public List<Car> getAll() {
        return this.carRepository.findAll();
    }

    @Override
    public Car createCar(Car car) {
        
        return this.carRepository.save(car);
    }

    @Override
    public Car updateCar(Car car) {
        Car existingCar = this.getById(car.getId());
        existingCar.setId(car.getId());
        existingCar.setMark(car.getMark());
        existingCar.setRegistration(car.getRegistration());
        existingCar.setYearOfConstuction(car.getYearOfConstuction());
        existingCar.setTechnician(car.getTechnician());
        return this.carRepository.save(existingCar);
        
    }

    @Override
    public void deleteCar(Integer id) {
        Car carToDelete = this.getById(id);
       this.carRepository.delete(carToDelete);
    }


}
