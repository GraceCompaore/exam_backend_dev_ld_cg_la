package com.example.Building.Co.service.serviceImpl;

import com.example.Building.Co.Repository.AddressRepository;
import com.example.Building.Co.entity.Address;
import com.example.Building.Co.exception.NotAllowedToDeleteCustomerException;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.service.AddressService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressRepository addressRepository;

    @Override
    public Address getById(Integer id) {
        return this.addressRepository
                .findById(id)
                .orElseThrow(() -> new UnknownResourceException("No address found with this ID"));
    }

    @Override
    public List<Address> getAll() {
        return this.addressRepository.findAll();
    }

    @Override
    public Address createAddress(Address address) {
        return this.addressRepository.save(address);
    }

    @Override
    public Address updateAddress(Address address) {
        Address existingAddress = this.getById(address.getId());
        existingAddress.setCityName(address.getCityName());
        existingAddress.setNumber(address.getNumber());
        existingAddress.setStreet(address.getStreet());
        existingAddress.setTechnicians(address.getTechnicians());
        existingAddress.setWorksites(address.getWorksites());
        return this.addressRepository.save(existingAddress);
    }

    @Override
    public void deleteAddress(Integer id) {
        Address addressToDelete = this.getById(id);
        if(this.canDeleteAddress(addressToDelete)){
            this.addressRepository.delete(addressToDelete);
        } else{
            throw new NotAllowedToDeleteCustomerException("The given address still has addresses");
        }
    }

    private boolean canDeleteAddress(Address address){
        return (null ==address.getTechnicians()
                || address.getTechnicians().isEmpty()
                || null ==address.getWorksites()
                || address.getWorksites().isEmpty()
        );
    }

}
