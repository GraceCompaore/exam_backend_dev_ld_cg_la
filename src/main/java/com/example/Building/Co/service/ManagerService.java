package com.example.Building.Co.service;

import com.example.Building.Co.entity.Manager;
import com.example.Building.Co.entity.Worksite;

import java.util.List;

public interface ManagerService {

    Manager getById(Integer id);
    List<Manager> getAll();
    Manager createManager(Manager manager);
    Manager updateManager(Manager manager);
    void deleteManager(Integer id);
}
