package com.example.Building.Co.service;

import com.example.Building.Co.entity.Technician;

import java.util.List;

public interface TechnicianService {

    List<Technician> getAll();

    Technician getById(Integer id);

    Technician createTechnician(Technician technician);

    void deleteTechnician(Integer id);

    Technician updateTechnician(Technician technician);
}
