package com.example.Building.Co.service.serviceImpl;

import com.example.Building.Co.Repository.ManagerRepository;
import com.example.Building.Co.entity.Manager;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.service.ManagerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ManagerServiceImpl implements ManagerService {
    Logger log = LoggerFactory.getLogger(ManagerServiceImpl.class);

    @Autowired
    private ManagerRepository managerRepository;

    @Override
    public List<Manager> getAll() {
        return this.managerRepository.findAll(Sort.by("lastname").ascending());
    }

    @Override
    public Manager getById(Integer id) {
        return managerRepository
                .findById(id)
                .orElseThrow(() -> new UnknownResourceException("No manager found for the given ID"));
    }

    @Override
    public Manager createManager(Manager manager) {
        log.debug("Attempting to save in DB...");
        return this.managerRepository.save(manager);
    }

    @Override
    public void deleteManager(Integer id) {
        Manager managerToDelete = this.getById(id);
        this.managerRepository.delete(managerToDelete);
    }

    @Override
    public Manager updateManager(Manager manager) {
        Manager existingmanager = this.getById(manager.getId());
        existingmanager.setFirstname(manager.getFirstname());
        existingmanager.setLastname(manager.getLastname());
        existingmanager.setMobile(manager.getMobile());
        existingmanager.setPhone(manager.getPhone());
        existingmanager.setTechnicians(manager.getTechnicians());

        return this.managerRepository.save(existingmanager);
    }
}
