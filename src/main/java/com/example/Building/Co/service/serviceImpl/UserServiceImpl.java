package com.example.Building.Co.service.serviceImpl;

import com.example.Building.Co.Repository.UserRepository;
import com.example.Building.Co.entity.User;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User getUserById(Integer id) {
        return this.userRepository
                .findById(id)
                .orElseThrow(() -> new UnknownResourceException("No user found with the given ID"));
    }

    @Override
    public List<User> getAll() {
        return this.userRepository.findAll(Sort.by("username").ascending());
    }

    @Override
    public User getByUsername(String username) {
        return this.userRepository.findByUsername(username)
                .orElseThrow();
    }

    @Override
    public User createUser(User user) {
        String passwordEncoded = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(passwordEncoded);
        return this.userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        User userToDelete = this.getUserById(id);
        this.userRepository.delete(userToDelete);

    }

    @Override
    public User updateUser(User user) {
        User userExistingToUpdate = this.getUserById(user.getId());
        userExistingToUpdate.setGrants(user.getGrants());
        userExistingToUpdate.setUsername(user.getUsername());
        String passwordEncoded = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(passwordEncoded);
        return this.userRepository.save(userExistingToUpdate);
    }

    @Override
    public User getUsernameAndPassword(String username, String password) {
        User user = this.userRepository.findByUsername(username).get();
        if (new BCryptPasswordEncoder().matches(password, user.getPassword())) {
            return user;
        }
        throw new UnknownResourceException("No user found for the given username and password");
    }
}
