package com.example.Building.Co.service.serviceImpl;


import com.example.Building.Co.Repository.WorksiteRepository;
import com.example.Building.Co.entity.Address;
import com.example.Building.Co.entity.Car;
import com.example.Building.Co.entity.Worksite;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.service.WorksiteService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class WorksiteServiceImpl implements WorksiteService {

    @Autowired
    private WorksiteRepository worksiteRepository;

    @Override
    public Worksite getById(Integer id) {

        return this.worksiteRepository
                .findById(id)
                .orElseThrow(()-> new UnknownResourceException("No worksite found with this ID"));
    }

    @Override
    public List<Worksite> getAll() {

        return this.worksiteRepository.findAll();
    }

    @Override
    public Worksite createWorksite(Worksite worksite) {
        return this.worksiteRepository.save(worksite);
    }

    @Override
    public Worksite updateWorksite(Worksite worksite) {

        Worksite existingWorksite = this.getById(worksite.getId());
        existingWorksite.setId(worksite.getId());
        existingWorksite.setName(worksite.getName());
        existingWorksite.setPrice(worksite.getPrice());
        existingWorksite.setAddress(worksite.getAddress());
        existingWorksite.setTechnicians(worksite.getTechnicians());
        return this.worksiteRepository.save(existingWorksite);
    }

    @Override
    public void deleteWorksite(Integer id) {
        Worksite worksiteToDelete = this.getById(id);
        this.worksiteRepository.delete(worksiteToDelete);

    }
}
