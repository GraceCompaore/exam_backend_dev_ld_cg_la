package com.example.Building.Co.service;

import com.example.Building.Co.entity.Car;
import com.example.Building.Co.entity.Manager;

import java.util.List;

public interface CarService {

    Car getById(Integer id);
    List<Car> getAll();
    Car createCar(Car car);
    Car updateCar(Car car);
    void deleteCar(Integer id);
}
