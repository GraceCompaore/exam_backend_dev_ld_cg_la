package com.example.Building.Co.service;

import com.example.Building.Co.entity.Address;
import com.example.Building.Co.entity.Manager;

import java.util.List;

public interface AddressService {
    Address getById(Integer id);
    List<Address> getAll();
    Address createAddress(Address address);
    Address updateAddress(Address address);
    void deleteAddress(Integer id);
}
