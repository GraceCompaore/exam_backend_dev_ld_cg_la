package com.example.Building.Co.service;


import com.example.Building.Co.entity.Worksite;

import java.util.List;


public interface WorksiteService {

    Worksite getById(Integer id);
    List<Worksite> getAll();
    Worksite createWorksite(Worksite worksite);
    Worksite updateWorksite(Worksite worksite);
    void deleteWorksite(Integer id);
}
