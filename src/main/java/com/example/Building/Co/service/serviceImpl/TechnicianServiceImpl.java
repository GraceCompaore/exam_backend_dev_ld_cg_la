package com.example.Building.Co.service.serviceImpl;

import com.example.Building.Co.Repository.TechnicianRepository;
import com.example.Building.Co.entity.Technician;
import com.example.Building.Co.exception.UnknownResourceException;
import com.example.Building.Co.service.TechnicianService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TechnicianServiceImpl implements TechnicianService {

    Logger log = LoggerFactory.getLogger(TechnicianServiceImpl.class);

    @Autowired
    private TechnicianRepository technicianRepository;

    @Override
    public List<Technician> getAll() {
        return this.technicianRepository.findAll();
    }

    @Override
    public Technician getById(Integer id) {
        return this.technicianRepository.findById(id).orElseThrow(() -> new UnknownResourceException("No technician found for the given id."));
    }

    @Override
    public Technician createTechnician(Technician technician) {
        //TODO Eviter qu'on puisse attribuer un véhicule déjà attribué à un autre technicien. Sinon ça casse le getAll() de l'API.
        return this.technicianRepository.save(technician);
    }

    @Override
    public void deleteTechnician(Integer id) {
        Technician technicianToDelete = this.getById(id);
        this.technicianRepository.delete(technicianToDelete);
    }

    @Override
    public Technician updateTechnician(Technician technician) {
        log.debug("Attempting to update technician {}", technician.getId());
        Technician existingTechnician = this.getById(technician.getId());
        existingTechnician.setFirstname(technician.getFirstname());
        existingTechnician.setLastname(technician.getLastname());
        existingTechnician.setAge(technician.getAge());
        existingTechnician.setAddress(technician.getAddress());
        existingTechnician.setManager(technician.getManager());
        existingTechnician.setCar(technician.getCar());
        return this.technicianRepository.save(existingTechnician);
    }

}
