package com.example.Building.Co.service;

import com.example.Building.Co.entity.User;

import java.util.List;

public interface UserService {

    User getUserById(Integer id);
    List<User> getAll();
    User getByUsername(String username);
    User createUser(User user);
    void deleteUser(Integer id);
    User updateUser(User user);

    User getUsernameAndPassword(String username, String password);



}
